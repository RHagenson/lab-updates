#  Lab-Updates

The VC repo I used during my time at UNO in pursuit of my Master of Biomedical Informatics degree.

I do not claim to own any of the multimedia stored in this repository. 
The LaTeX source and its compiled results are owned by me, but free to repurpose however one pleases. 
If a substational amount is used from this source, please credit me in a manner fitting the context of its use.