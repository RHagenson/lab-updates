\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{General Overview}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Intro}{2}{0}{1}
\beamer@sectionintoc {2}{Background/Refresher}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Intro}{9}{0}{2}
\beamer@sectionintoc {3}{Protein Disorder}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{Intro}{12}{0}{3}
\beamer@sectionintoc {4}{Methodology}{14}{0}{4}
\beamer@subsectionintoc {4}{1}{Intro}{14}{0}{4}
\beamer@sectionintoc {5}{Wrap-up}{18}{0}{5}
\beamer@subsectionintoc {5}{1}{Intro}{18}{0}{5}
