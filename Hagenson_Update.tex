%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Laboratory Update                            %
% Ryan Hagenson                                %                                                                                                                     %                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[xcolor=dvipsnames,table]{beamer}
\usepackage{etex}
\mode<presentation>

% define your own colors:
\definecolor{unoRed}{RGB}{215, 25, 32}
\definecolor{unoDarkGray}{RGB}{99,101,104}
\definecolor{magenta}{RGB}{1,0,.6}
\definecolor{lightblue}{RGB}{0,.5,1}
\definecolor{lightpurple}{RGB}{.6,.4,1}
\definecolor{gold}{RGB}{.6,.5,0}
\definecolor{orange}{RGB}{1,0.4,0}


\long\def\symbolfootnote[#1]#2{\begingroup%
\def\thefootnote{\fnsymbol{footnote}}\footnote[#1]{#2}\endgroup}

\usetheme{Warsaw}
\usecolortheme[RGB={99,101,104}]{structure}
\setbeamercolor{subsection in head/foot}{fg=white,bg=unoRed}

%\hypersetup{pdfpagemode=FullScreen} % makes your presentation go automatically to full screen

\useoutertheme[subsection=false]{smoothbars}


% include packages
\usepackage{subfigure}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage[all,knot]{xy}
\xyoption{arc}
\usepackage{url}
\usepackage{ulem}
\usepackage{multimedia}
\usepackage{hyperref}
\usepackage{etex}
\usepackage{media9}
\usepackage{algorithm,algorithmic}

%\logo{\includegraphics[width=1.8cm]{figures/UNOColor.pdf}}
\title{Hagenson Laboratory Update -- \today}
\subtitle{Protein Disorder in Cancer and its Effects on Binding Relationships}
\author{Ryan Hagenson (rhagenson@unomaha.edu)}
\institute{School of Interdisciplinary Informatics\\University of Nebraska at Omaha}
\date{\today}

\begin{document}

\frame{
  \titlepage
}

\section{General Overview}
\subsection{Intro}

\frame{
	\frametitle{Previous studies}
	\begin{itemize}
	\item Historical focus on ordered regions
	\item Binding sites were the primary regions of interest
	\item Mutations are destabilizing
	\end{itemize}
}

\frame{
	\frametitle{Shift in focus}
	\begin{itemize}
	\item New focus on disorder regions
	\item Inherently disordered regions (IDRs) primary regions of interest
	\item How are IDRs affected by generally destabilizing mutations?
	\end{itemize}
}

\frame{
	\frametitle{IDRs and what they provide}
	\begin{center}
	\includegraphics[scale=0.3]{figures/DisorderBabu.png}\\
	\tiny Babu MM, Kriwacki RW, Pappu R V. Versatility from Protein Disorder. Science (80- ) 2012;337(6101):1460–1.
	\end{center}
}

\frame{
  \frametitle{IDRs and what they provide (continued)}
  \begin{multicols}{2}
  	\begin{center}
    	\includegraphics[width=\linewidth]{figures/DisorderBabu.png}\\
		\tiny Babu MM, Kriwacki RW, Pappu R V. Versatility from Protein Disorder. Science (80- ) 2012;337(6101):1460–1.
  	\end{center}
  	\columnbreak
  	\begin{center}
		\begin{itemize}
    	\item Adds robust function to protein networks
    	\item Complements the rigidity of binding sites
    	\item Provides inexact binding relationships
  		\end{itemize}
  	\end{center}
  \end{multicols}
}

\frame{
	\frametitle{Termini and intermediate disorder}
	\begin{columns}[T]
		\begin{column}{.5\textwidth}
			\begin{block}{Termini positions}
				\begin{itemize}
				\item Far more disorder
				\item Folding leaves them free to move
				\item More hydrophillic
				\end{itemize}
    		\end{block}
    	\end{column}
    	\begin{column}{.5\textwidth}
			\begin{block}{Intermediate positions}
				\begin{itemize}
				\item Far more ordered
				\item Folding leaves them stuck in place
				\item More hydrophobic
				\end{itemize}
    		\end{block}
    	\end{column}
  \end{columns}
}

\frame{
	\frametitle{Binding relationships}
	\begin{itemize}
	\item IDRs have no set 3D structure
	\item IDRs do have set binding partners
	\item How do mutations disrupt these relationships?
	\end{itemize}
}

\frame{
	\frametitle{Proposed outcomes}
	\begin{block}{Proposed outcomes}
		\begin{quotation}
		I propose that by studying the effects of cancer mutations within 
		IDRs, we can further understand how cancer manipulates cellular 
		chemistry – disrupting healthy, normal processes. This relatively 
		unstudied half of the binding partners may lead to
		new means of cancer intervention or discovery of new driver genes.
		\end{quotation}
    \end{block}
}


\section{Background/Refresher}
\subsection{Intro}

\frame{
	\frametitle{Cancer Hallmarks}
	\begin{block}{Hanahan-Weinberg Hallmarks}
	\begin{enumerate}
	\item Sustaining proliferative signaling
	\item Evading growth suppressors
	\item Activating invasion and metastasis
	\item Enabling replicative immortality
	\item Inducing angiogenesis
	\item Resisting cell death
	\end{enumerate}
	\end{block}
}

\frame{
	\frametitle{Mutations in cancer}
	\begin{itemize}
	\item In simplest terms: cancer arises from somatic mutation events\footnote{\tiny Hanahan D, Weinberg RA. Hallmarks of cancer: the next generation. Cell 2011;144(5):646–74.}
	\item Destabilizing binding site mutations stunt or accelerate protein activity
	\item Can any of these effects be linked to mutations in IDRs? 
	\end{itemize}
}

\frame{
	\frametitle{Isoforms}
	\begin{itemize}
	\item Different end points of the same gene
	\item Can have different binding site and/or IDR sequences
	\item Can act at individual "barometers" for what has occurred upstream from them
		\begin{itemize}
		\item That is to say, not all mutations affect all isoforms equally
		\end{itemize}
	\item Mutations propagate to this level unequally
	\end{itemize}
}

\section{Protein Disorder}
\subsection{Intro}

\frame{
	\frametitle{What is protein disorder?}
	\begin{itemize}
	\item The measure of how well we know the 3D conformation
	\item Can be measured on a per-position basis
	\item Is the result of interaction effects with other amino acids
	\end{itemize}
}

\frame{
	\frametitle{IUPred disorder measurement method}
	\begin{enumerate}
	\item Originally designed to discriminate intrinsically unstructured proteins (IUPs) from globular proteins
	\item Uses a sliding window to determine number of interaction to consider
		\begin{itemize}
		\item IUPred 'long' is 100 residue window
		\item IUPred 'short' is 25 residue window
		\item Both smoothed over at 21 residue window
		\end{itemize}
	\item Each position is the summation of all interaction its energies
	\item Values from from $0 - 1$, with 1 being complete disorder
	\item Precise to the ten-thousandth position (high granularity)
	\item Can distinguish partially disorder from fully disordered proteins
	\item Currently the best method with correlation coefficient between calculated and measured energies of $0.76$
	\end{enumerate}
}

\section{Methodology}
\subsection{Intro}

\frame{
	\frametitle{Data}
	\begin{columns}[T]
		\begin{column}{.5\textwidth}
			\begin{block}{Acquisition}
				\begin{itemize}
				\item Obtained from Broad Institute Firehose system
				\item Format: Data Level 2 (\textit{Processed Data})
				\item Date obtained: July 18th, 2016
				\end{itemize}
    		\end{block}
    	\end{column}
    	\begin{column}{.5\textwidth}
			\begin{block}{Size}
				\begin{itemize}
				\item $95,836$ proteins
				\item Each processed twice (concurrently, but separately)
					\begin{itemize}
					\item Via IUPred 'long'
					\item Via IUPred 'short
					\end{itemize}
				\item 31 cancer types
				\end{itemize}
    		\end{block}
    	\end{column}
  \end{columns}
}

\frame{
	\setcounter{footnote}{0}
	\frametitle{Disordered positions}
	\begin{block}{Monte Carlo simulation}
		\begin{itemize}
		\item Last semester, ran a 3 month Monte Carlo simulation\footnote[frame]{\tiny In R, but hindsight says I should have done it in C}
		\item FDR correction with $p_{adj} < 0.05$ cutoff
			\begin{itemize}
			\item 71 significant hits
			\item 40 significant genes
			\item 16 cancer types
			\end{itemize}
		\item Now performing enrichment analysis
			\begin{itemize}
			\item Via hypergeometric method
			\item Via Gene Set Enrichment Analysis (GSEA) method
			\end{itemize}
		\end{itemize}
	\end{block}
}

\frame{
	\setcounter{footnote}{0}
	\frametitle{Disordered regions}
	\begin{block}{Plan}
	\begin{itemize}
	\item Use FoldIndex API to determine disordered regions
	\item Use a modified e-Driver\footnote[frame]{\tiny Porta-Pardo E, Godzik A. E-Driver: A novel method to identify protein regions driving cancer. Bioinformatics 2014 Nov 1;30(21):3109–14.} method to measure significance of mutations
		\begin{itemize}
		\item Uses binomial distribution to test for clustering inside disorder regions
		\item FDR correction with $p_{adj} < 0.05$ cutoff
		\item Likely will see sequential clustering as well as 3D clustering
		\end{itemize}
	\item No ability to map to 3D structure as with highly-ordered regions
	\end{itemize}
	\end{block}
}

\frame{
	\setcounter{footnote}{0}
	\frametitle{Validation}
	\begin{block}{Methods}
	\begin{itemize}
	\item Enrichment analysis as validation
	\item Ensure limited union with highly-ordered analysis sets
	\item Cross-reference with cancer Gene Census\footnote[frame]{\tiny http://cancer.sanger.ac.uk/census}
	\item Potential wet-lab validation eventually needed
	\item \textbf{Build unique binding-partner set and validate that set via these methods}
	\end{itemize}
	\end{block}
}

\section{Wrap-up}
\subsection{Intro}

\frame{
	\frametitle{Preliminary results/currently working on}
	\begin{itemize}
	\item Significant hits from Monte Carlo simulation
	\item Enrichment analysis has not shown significance (having issues)
	\item Working on using FoldIndex API to generate new starting point (disorder regions analysis)
	\end{itemize}
}

\frame{
	\frametitle{Comments?}
	What would you do differently and what can I be doing better?
}

\frame{
	\frametitle{My Major Question}
	Can anyone think of how I might simulate or otherwise measure the effect mutations have on binding affinity with all partners?
}

%\frame{
%	\frametitle{Isoforms}
%	\begin{block}{Isoforms, many, many isoforms}
%	\begin{itemize}
%	\item 95,836 proteins
%	\item 31 cancer types
%	\end{itemize}
%	\end{block}
%}

% Text and image side-by-side
%\frame{
%  \frametitle{IDRs and what they provide}
%	\begin{columns}[T]
%		\begin{column}{.5\textwidth}
%			\begin{block}{Summary}
%	
%    		\end{block}
%    	\end{column}
%    	\begin{column}{.5\textwidth}
%    		\begin{block}{Your image}
%    			\includegraphics[scale=0.5]{figures/DisorderBabu.png}\\
%    			\tiny Babu MM, Kriwacki RW, Pappu R V. Versatility from Protein Disorder. Science (80- ). 2012;337(6101):1460–1. 
%    		\end{block}
%    	\end{column}
%  \end{columns}
%}

%% slide
%\frame{
%  \frametitle{Learning outcomes}
%  \begin{center}
%    \includegraphics[width=8cm]{figures/bigIdeas.png}
%  \end{center}
%  \symbolfootnote[0]{\tiny http://ctl.utexas.edu/teaching/course-design/learning-outcomes}
%}
%
%% slide
%\frame{
%  \frametitle{Learning outcomes of this course}
%  \scriptsize
%  \begin{itemize}
%   \item An appreciation of the past, present, and future of Public Health Genomics.
%   \item An understanding of the biological mechanisms of mutations, and their implication for inherited and acquired disorders.
%   \item Knowledge of the basic mechanisms of inheritance, inheritance patterns, and family pedigrees.
%   \item An understanding of current Genomics techniques, with an emphasis on the computational methods that are needed to process and interpret the data.
%   \item An appreciation of the complex interplay between genetic makeup of an individual and environment.
%   \item An understanding of the biological, social, and ethical aspects of preconceptional genetic screening.
%   \item An understanding of the biological, social, and ethical aspects of newborn screening.
%   \item An understanding of the biological, social, and ethical aspects of adult genetic counseling.
%   \item An appreciation of human diversity and the new frontiers of personalized/precision Medicine. 
%   \item An appreciation of Genomics in the context of communicable disease\\control and biothreat surveillance.
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{The Syllabus: the content of the course}
%  \scriptsize
%  \begin{enumerate}
%    \item Past, present and future of Public Health Genomics (1 week).
%    \item Molecular genetics, genome sequencing projects, current experimental and computational techniques in Genomics (2 weeks).
%    \item Mutations, inheritance patterns, family pedigrees, and genetic disorders (1 week).
%    \item Population genetics and ethnicity (1 week).
%    \item The Nature vs. Nurture debate: a genomics perspective. Genome-wide association studies (2 weeks).
%    \item Preconceptional genetic screening (1 week).
%    \item Newborn screening (1 week).
%    \item Adult genetic counseling (1 week).
%    \item Pharmacogenomics (1 week).
%    \item Communicable disease control and biothreat surveillance: a genomics perspective (1 week).
%    \item Genetic information, ethics, and the law: current and future challenges (1 week).
%  \end{enumerate}
%}
%
%% slide
%\frame{
%  \frametitle{Assessment}
%  \begin{itemize}
%    \item mid term exam (20\% of the grade)
%    \vspace{0.2cm}
%    \item final exam (20\% of the grade)
%    \vspace{0.2cm}
%    \item one paper on a mutually agreed upon topic (20\% of the grade)
%    \vspace{0.2cm}
%    \item an oral presentation (20\% of the grade)
%    \vspace{0.2cm}
%    \item assignments (20\% of the grade)
%    \vspace{0.2cm}
%    \item \textbf{class participation} (for example, A- $\rightarrow$ A)
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{The textbook(s)}
%  \begin{center}
%    \includegraphics[width=4cm]{figures/textbook1.png}
%    \hspace{0.3cm}
%    \includegraphics[width=4cm]{figures/textbook2.png}
%  \end{center}
%}
%
%% slide
%\frame{
%  \frametitle{Learning objectives for today}
%  \begin{itemize}
%    \item Develop an appreciation of the history of genetics, genomics, public health, and their relationship
%    \item Provide concrete examples to illustrate how advances in genomics can foster ``precision'' medicine
%    \item Describe some of the future challenges that have to be faced by the ``-omics'' disciplines
%  \end{itemize}
%}
%
%\section{A brief history of genetics}
%\subsection{Intro}
%
%% slide
%\frame{
%  \frametitle{Hereditary bleeding disorders in the Talmud}
%  \scriptsize
%  \begin{block}{Abstract from \textit{Ann Intern Med.} 1969;70(4):833-837}
%  ``A familial bleeding disorder, probably hemophilia, is described in the Talmud. The decree of Rabbi Judah\footnote{\scriptsize 2nd century CE} that the sibling of two brothers who died of bleeding after circumcision may not be circumcised is codified by Jewish sages of the last 10 centuries. Such rulings are found in the works of Alfasi (eleventh century); Maimonides (twelfth century)\footnote{\scriptsize expanded on Rabbi Judah's ruling, and added that even if the first two sons who died after circumcision were from different husbands, the third son should not be circumcised. } [...]
%This ruling was stated only in regard to siblings or maternal cousins as only the direct maternal transmission of the disease was recognized. Omitted from all the Jewish sources is a consideration of the child whose maternal uncles died of bleeding after circumcision.''
%  \end{block}
%}
%
%% slide
%\frame{
%  \frametitle{Hereditary bleeding disorders in the Islamic Golden Age}
%  \begin{itemize}
%    \item Abu al-Qasim Khalaf ibn al-Abbas Al-Zahrawi (936-1013)
%    \item lived in Andalusia, in the south of modern day Spain
%    \item known as one of the fathers of surgery
%    \item describes the sex-linked inheritance of hemophilia\footnote{\tiny Falagas M. et al., ``Arab science in the golden age (750--1258 C.E.) and today'', \textit{FASEB Journal}, 2006, 20(10):1581-1586}
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{Mendel's laws of inheritance}
%  \begin{center}
%    \includegraphics[width=3cm]{figures/mendel.png}
%  \end{center}
% 
%  \emph{Gregor Mendel}, 1822-1884
%  \begin{itemize}
%    \item one of the fathers of genetics
%    \item by experimenting with pea plants he discovered the main principles of inheritance
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{The first links between genetics and human disease}
%  \begin{center}
%    \includegraphics[width=2.5cm]{figures/garrod.png}
%    \includegraphics[width=3.5cm]{figures/inborn.png}
%  \end{center}
%  \begin{itemize}
%    \item Sir Archibal Garrod discovers that alkaptonuria (blackening of the urine
%    exposed to the air and arthritis) has a hereditary component (autosomal recessive), and
%    publishes his results in 1902
%    \item subsequently he establishes that together with alkaptonuria, albinism,  cystinuria, and pentosuria are also hereditary
%    (Garrod's tetrad)
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{A timeline of genetic discovery}
%  \begin{center}
%    \includegraphics[width=12.1cm]{figures/historyGenetics.pdf}
%  \end{center}
%}
%
%\section{A brief history of Public Health}
%\subsection{Intro}
%
%% slide
%\frame{
%  \frametitle{Medicine in the community}
%  \begin{block}{From Bynum's \textit{The History of Medicine}}
%  ``The modern public health movement began in the 19th century. [...]
%  If the relationship between patients and their doctors situates hospital medicine,
%  public health is about the state and the individual. It is at once the most anonymous
%  part of medicine and the most visible. When we go to the hospital, not many people
%  notice. When there is an outbreak of influenza, or the water supply is contaminated,
%  it is newsworthy.``
%  \end{block}
%}
%
%% slide
%\frame{
%  \frametitle{The Black Death and public health awareness}
%  \begin{center}
%    \includegraphics[width=6cm]{figures/blackdeath.png}
%  \end{center}
%  \begin{itemize}
%    \item despite the range of supernatural explanations for it, the repeated
%    plague epidemics raised awareness of communal health issues
%    \item some scholars believe that quarantine models along the edges of the
%    Austro-Hungarian Empire might have helped keeping the West plague-free even
%    as the disease continued to be endemic in the Middle East
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{Cholera and poverty: motors of public health}
%  \begin{itemize}
%    \item the first cholera epidemics that reached Europe in the early XIX century
%    raised consciousness about communal diseases
%    \vspace{0.3cm}
%    \item the pattern of spread was puzzling. The prevailing models of the time (\emph{miasmatic}
%    -- through the air -- and \emph{contagious} -- from one individual to another)
%    were both inadequate to explain the spread of cholera
%    \vspace{0.3cm}
%    \item at the same time, the New Poor Law in England emphasized
%    the importance of the environment in the higher rate of diseases that affected
%    the poor
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{The mechanism for the spread of cholera is finally elucidated}
%  \begin{itemize}
%    \item Italian microscopist Filippo Pacini (1812 - 83) described the causative agent of
%    cholera (the bacterium \textit{Vibrio cholerae})
%    \vspace{0.3cm}
%    \item John Snow (1813 - 58), a London anesthesiologist and epidemiologist, provided
%    conclusive evidence showing that cholera spreads through contaminated water
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{Jenner and the first vaccine}
%  \begin{center}
%    \includegraphics[width=3cm]{figures/jenner.png}\\
%    \tiny Edward Jenner's commemorarive U.K. stamp
%  \end{center}
%  \begin{itemize}
%    \item Edward Jenner (1749 - 1823) noticed that an affliction of cows, cowpox,
%    occasionally caused a small lesion on the hands of the milkmaids, who seemed to
%    be protected from the much more severe smallpox
%    \item in 1796 he performed the crucial experiment and started advertising this
%    new preventative, which will become\\known as \emph{vaccine} (from the Latin adjective
%    for cow)
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{Vaccination becomes compulsory}
%   \begin{center}
%    \includegraphics[width=4cm]{figures/calkin.png}\\
%    \tiny The public vaccinator, by Lance Calkin (c. 1901)
%   \end{center}
%  The leader of the British Public Health Movement, John Simon (1816 - 1904), promoted
%    a vaccination system that was\\ publicly funded, free, universal, and compulsory
%}
%
%% slide
%\frame{
%  \frametitle{Cancer death rates (males)}
%    \begin{center}
%    \includegraphics[width=9cm]{figures/deathRatesMales.png}
%  \end{center}
%}
%
%% slide
%\frame{
%  \frametitle{Cancer death rates (females)}
%  \begin{center}
%    \includegraphics[width=9cm]{figures/deathRatesFemales.png}
%  \end{center}
%}
%
%% slide
%\frame{
%  \frametitle{A classic of Public Health}
%  \begin{center}
%    \includegraphics[width=9cm]{figures/britishJournal.png}
%  \end{center}
%}
%
%\section{Public Health Genomics}
%\subsection{Intro}
%
%% slide
%\frame{
%  \frametitle{Attempting a definition}
%  \begin{block}{What is Public Health Genomics?}
%   Public Health Genomics is the application of genetics, genomics and other ``-omics'' disciplines
%    to promote public health by preventing disease, enabling the early detection of disease,
%    and improving the management of existing disease.
%  \end{block}
%}
%
%% slide
%\frame{
%  \frametitle{Levels of prevention}
%  \begin{center}
%    \includegraphics[width=7cm]{figures/levelsOfPrevention}
%  \end{center}
%  \symbolfootnote[0]{\tiny Source: http://pamodules.mc.duke.edu}
%  Genomics can contribute to each of these three levels of prevention.
%}
%
%% slide
%\frame{
%  \frametitle{Pharmacogenetics: the case of warfarin}
%  \begin{multicols}{2}
%  \begin{center}
%    \includegraphics[width=4cm]{figures/warfarin.png}
%  \end{center}
%  \columnbreak
%  \begin{itemize}
%    \item anticoagulant originally intended as rat poison (!)
%    \item fairly narrow therapeutic index, with variability in patients' response
%    to the same dosage
%    \item few genetic variants responsible for a good portion of the interpatient variability
%    \item is it a good candidate for genetic testing?
%  \end{itemize}
%  \end{multicols}
%}
%
%% slide
%\frame{
%  \frametitle{Pharmacogenetics: the case of warfarin (2)}
%  \begin{itemize}
%    \item A paper that was published in 2008 by Flockhart et al\footnote{\tiny Flockhart D.A. et al, \textit{Genetics in Medicine} 2008, 10(2):139-150} suggested that genetic testing
%    might be useful before prescribing the drug
%    \item However, a newer study published in 2013\footnote{\tiny Kimmel S.E. et al, \textit{N Engl J Med} 2013, 369(24):2283-2293} seems to contradict that recommendation, concluding that
%    ``genotype-guided dosing of warfarin did not improve anticoagulation control during the first 4 weeks of therapy'' over a more
%    traditional clinical algorithm.
%  \end{itemize}
%}
%
%% slide
%\frame{
%  \frametitle{U.S. Preventive Services Task Force}
%  \begin{center}
%    \includegraphics[width=10cm]{figures/grades.png}
%  \end{center}
%  \symbolfootnote[0]{\tiny http://www.uspreventiveservicestaskforce.org/Page/Name/grade-definitions}
%}
%
%% slide
%\frame{
%  \frametitle{U.S. Preventive Services Task Force (2)}
%  \begin{center}
%    \includegraphics[width=9.5cm]{figures/certainty.png}
%  \end{center}
%  \symbolfootnote[0]{\tiny http://www.uspreventiveservicestaskforce.org/Page/Name/grade-definitions}
%}
%
%% slide
%\frame{
%  \frametitle{The example of hemochromatosis}
%  \begin{itemize}
%    \item \textbf{hemochromatosis} is a disorder of iron storage with increased intestinal iron absorption
%    \item the disease affects the liver, the pancreas, the joints, and the heart
%    \item there is an acquired form and an \textbf{hereditary} form, which involves the \textit{HFE} gene
%    \item the HFE gene is involved in iron sensing and in regulating intestinal absorption
%    \item two mutations (C282Y and H63D) explain the majority of hereditary cases
%  \end{itemize}
%  \textcolor{red}{Should there be a screening for hereditary hemochromatosis?}
%}
%
%% slide
%\frame{
%  \frametitle{The example of hemochromatosis (2)}
%  \begin{center}
%    \includegraphics[width=9.5cm]{figures/annalsHemo.png}
%  \end{center}
%}
%
%\section{The future}
%\subsection{Intro}
%
%% slide
%\frame{
%  \frametitle{The future of genomics}
%  \begin{center}
%    \includegraphics[width=8cm]{figures/futureGenomics.png}
%  \end{center}
%  \symbolfootnote[0]{\tiny ELSI: Ethical, Legal and Social Implications (ELSI) Research Program}
%  \symbolfootnote[0]{\tiny Collins F.S. et al, ``A vision for the future of genomics research'', \textit{Nature} 2003, 422:835-847}
%}
%
%% slide
%\frame{
%  \frametitle{Grand challenges: genomics to biology}
%  From Collins F.S. et al, ``A vision for the future of genomics research'', \textit{Nature} 2003, 422:835-847
%  \vspace{0.4cm}
%  \begin{enumerate}
%    \item Comprehensively identify the structural and functional components encoded in the human genome
%    \pause
%    \item Elucidate the organization of genetic networks and protein pathways and establish how they contribute to cellular and organismal phenotypes
%    \pause
%    \item Develop a detailed understanding of the heritable variation in the human genome
%    \pause
%    \item Understand evolutionary variation across species and the mechanisms underlying it
%    \pause
%    \item Develop policy options that facilitate the widespread use of genome information in both research and clinical settings
%  \end{enumerate}
%}
%
%% slide
%\frame{
%  \frametitle{Grand challenges: genomics to health}
%  \begin{enumerate}
%    \item Develop robust strategies for identifying the genetic contributions to disease and drug response
%    \pause
%    \item Develop strategies to identify gene variants that contribute to good health and resistance to disease
%    \pause
%    \item Develop genome-based approaches to prediction of disease susceptibility and drug response, early detection of illness, and molecular taxonomy of disease states
%    \pause
%    \item Use new understanding of genes and pathways to develop powerful new therapeutic approaches to disease
%    \pause
%    \item 
%    Investigate how genetic risk information is conveyed in clinical settings [...]
%    \pause
%    \item Develop genome-based tools that improve the health\\of all
%  \end{enumerate}
% }
% 
% % slide
% \frame{
%   \frametitle{Grand challenges: genomics to society}
%   \begin{enumerate}
%     \item Develop policy options for the uses of genomics in medical and non-medical settings
%     \pause
%     \item Understand the relationships between genomics, race and ethnicity, and the consequences of uncovering these relationships
%     \pause
%     \item Understand the consequences of uncovering the genomic contributions to human traits and behaviors
%     \pause
%     \item Assess how to define the ethical boundaries for uses of genomics
%   \end{enumerate}
% }
% 
% % slide
%\frame{
%  \frametitle{Future in the making: Cancer Genome Research}
%  Goals
%  \begin{itemize}
%    \item identify changes in the genomes of tumors that drive cancer progression
%    \item identify new targets for therapy
%    \item select drugs based on the genomics of the tumor
%  \end{itemize}
%  \symbolfootnote[0]{\tiny Source: ICGC.org}
%}
%
%% slide
%\frame{
%  \frametitle{The beginnings of cancer genomics}
%  \begin{center}
%    \includegraphics[width=9cm]{figures/Slide05.png}
%  \end{center}
%}
%
%% slide
%\frame{
%  \frametitle{What did we learn from those early studies?}
%  \begin{itemize}
%    \item \textbf{heterogeneity} within and across tumor types
%    \item high rate of abnormalities (driver vs. passenger)
%    \item sample quality matters
%  \end{itemize}
%  \symbolfootnote[0]{\tiny Source: ICGC.org}
%}
%
%% slide
%\frame{
%  \frametitle{Why do we need international efforts?}
%  \pause
%  \includegraphics[width=9cm]{figures/Slide13.png}
%}
%
%% slide
%\frame{
%  \frametitle{ICGC samples growth over time}
%  \begin{center}
%    \includegraphics[width=9cm]{figures/samplesOverTime.png}
%  \end{center}
%  \symbolfootnote[0]{\tiny Source: \href{https://icgc.org}{\textcolor{blue}{ICGC.org}}}
%}
%
%% slide
%\frame{
%  \frametitle{The promises of Cancer Genomics}
%  \begin{itemize}
%    \item better identification of risk factors, hereditary component of cancers, etc.
%    \item better stratification of cancer patients (based on the actual genetic make-up of the tumors)
%    \item targeted therapies (``precision'' medicine)
%  \end{itemize}
%}

\end{document}